import React from "react";
import { Link } from "react-router-dom";
import "../App.css";
import { useStore } from "../Store/Store";

const RouterMenu = () => {
  const { Menu, selected } =
    useStore();
  return (
    <div className="menuContainer">
      {Menu.map((item, index) => (
        <Link style={{ textDecoration: "none" }} to={item.link}>
          <div
            style={{
              height: "70%",
              width: 200,
              backgroundColor: index === selected ? "#9FA8DA" : "#303f9f",
              borderRadius: 10,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              cursor: "pointer",
            }}
          >
            <p
              style={{
                fontSize: 18,
                fontWeight: "bold",
                userSelect: "none",
                color: "white",
              }}
            >
              {item.name}
            </p>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default RouterMenu;
