import React from "react";
import "../App.css";

const Card = ({num,cvv2,exp,owner,value,onClick,style}) => {
  return (
    <div className="cartBox" style={{...style}} onClick={onClick}>
      <p
        style={{
          color: "white",
          fontSize: 25,
          fontWeight: "bold",
          userSelect: "none",
        }}
      >
        {num}
      </p>
      <div
        style={{
          height: 15,
          width: "70%",
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <p
          style={{
            color: "white",
            fontSize: 20,
            fontWeight: "bold",
            userSelect: "none",
          }}
        >
          cvv2 : {cvv2}
        </p>
        <p
          style={{
            color: "white",
            fontSize: 20,
            fontWeight: "bold",
            userSelect: "none",
          }}
        >
          exp date : {exp}
        </p>
      </div>
      <div
        style={{
          height: 15,
          width: "90%",
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <p
          style={{
            color: "white",
            fontSize: 18,
            fontWeight: "bold",
            userSelect: "none",
          }}
        >
          {owner}
        </p>
        <p
          style={{
            color: "white",
            fontSize: 18,
            fontWeight: "bold",
            userSelect: "none",
          }}
        >
          موجودی : {value}
        </p>
      </div>
    </div>
  );
};

export default Card;
