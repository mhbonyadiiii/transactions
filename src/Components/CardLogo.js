import React from "react";
import Mellat from "../images/Mellat.png";
import Saman from "../images/Saman.png";
import Melli from "../images/Melli.png";
import Saderat from "../images/Saderat.png";
import Sepah from "../images/Sepah.png";
import Parsian from "../images/Parsian.png";
import Tejarat from "../images/Tejarat.png";
import Pasargad from "../images/Pasargad.png";
import Keshavarzi from "../images/Keshavarzi.png";
import { useStore } from "../Store/Store";

const CardLogo = ({ id }) => {
  const { valid, setValid } = useStore();

  switch (id) {
    case "603799": {
      setValid(true);
      return <img src={Melli} style={{ width: 40, height: 50 }} />;
    }
    case "603769": {
      setValid(true);

      return <img src={Saderat} style={{ width: 40, height: 50 }} />;
    }
    case "621986": {
      setValid(true);

      return <img src={Saman} style={{ width: 50, height: 50 }} />;
    }
    case "610433": {
      setValid(true);

      return <img src={Mellat} style={{ width: 50, height: 40 }} />;
    }
    case "603770": {
      setValid(true);

      return <img src={Keshavarzi} style={{ width: 50, height: 40 }} />;
    }
    case "502229": {
      setValid(true);

      return <img src={Pasargad} style={{ width: 50, height: 40 }} />;
    }
    case "627353": {
      setValid(true);

      return <img src={Tejarat} style={{ width: 40, height: 40 }} />;
    }
    case "622106": {
      setValid(true);

      return <img src={Parsian} style={{ width: 40, height: 40 }} />;
    }
    case "589210": {
      setValid(true);

      return <img src={Sepah} style={{ width: 40, height: 40 }} />;
    }

    default: {
      setValid(false);
      return null;
    }
  }
};

export default CardLogo;
