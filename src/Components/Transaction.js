import React, { useEffect, useState } from "react";
import "../App.css";
import { useStore } from "../Store/Store";

const Transaction = () => {
  const {
    Carts,
    CardNum,
    setCardNum,
    CVV2Num,
    setCVV2Num,
    ExpNum,
    setExpNum,
    transValue,
    setTransValue,
    DestCard,
    setDestCard,
    allValueCompute,
    selected,
    setSelected,
    Password,
    setPassword,
    passRef,
    transRef,
    destRef,
    CardNumRef,
    CVV2NumRef,
    ExpNumRef,
  } = useStore();

  useEffect(() => {
    setSelected(1);
  }, []);

  const submitTrans = () => {
    let temp = [...Carts];
    if (
      CardNum &&
      CVV2Num &&
      ExpNum &&
      transValue &&
      Password.length >= 5 &&
      DestCard.length === 16
    ) {
      temp.find((elem) => {
        if (elem.Num === CardNum) {
          if (elem.Value - transValue >= 0) {
            let TMP = elem.Transactions;
            TMP.push({
              cardNum: CardNum,
              transValue: transValue,
              destCard: DestCard,
            });
            const obj = elem.Value;
            allValueCompute(CardNum, "Value", obj - transValue);
            alert("تراکنش موفق");
            setCardNum();
            setCVV2Num();
            setExpNum();
            setPassword([]);
            setTransValue();
            setDestCard([]);
            passRef.current.value = "";
            transRef.current.value = "";
            destRef.current.value = "";
            CardNumRef.current.value = "";
            CVV2NumRef.current.value = "";
            ExpNumRef.current.value = "";
          } else {
            alert("موجودی کافی نیست");
          }
        }
      });
    } else alert("اطالاعات تراکنش را کامل کنید");
  };

  return (
    <div className="cartInputbox" style={{ backgroundColor: "#5E35B1" }}>
      <div className="cartInput">
        <input
          className="Input"
          maxLength={16}
          value={CardNum}
          ref={CardNumRef}
          disabled
          placeholder="کارت خود را انتخاب کنید"
        />
        <div className="inputTitleBox" style={{ backgroundColor: "#5E35B1" }}>
          <p className="inputTitle">شماره کارت</p>
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          maxLength={4}
          value={CVV2Num}
          ref={CVV2NumRef}
          disabled
          placeholder="کارت خود را انتخاب کنید"
        />
        <div className="inputTitleBox" style={{ backgroundColor: "#5E35B1" }}>
          <p className="inputTitle">CVV2</p>
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          maxLength={5}
          value={ExpNum}
          ref={ExpNumRef}
          disabled
          placeholder="کارت خود را انتخاب کنید"
        />
        <div className="inputTitleBox" style={{ backgroundColor: "#5E35B1" }}>
          <p className="inputTitle">تاریخ انقضا</p>
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          onChange={(e) => setPassword(e.target.value)}
          ref={passRef}
          value={Password}
        />
        <div className="inputTitleBox" style={{ backgroundColor: "#5E35B1" }}>
          <p className="inputTitle">رمز دوم</p>
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          onChange={(e) => setTransValue(e.target.value)}
          ref={transRef}
          value={transValue}
        />
        <div className="inputTitleBox" style={{ backgroundColor: "#5E35B1" }}>
          <p className="inputTitle">مبلغ انتقال</p>
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          onChange={(e) => setDestCard(e.target.value)}
          type="text"
          onKeyPress={(event) => {
            let char = String.fromCharCode(event.which);
            if (!/[0-9]/.test(char)) {
              event.preventDefault();
            }
          }}
          maxLength="16"
          ref={destRef}
          value={DestCard}
        />
        <div className="inputTitleBox" style={{ backgroundColor: "#5E35B1" }}>
          <p className="inputTitle">کارت مقصد</p>
        </div>
      </div>
      <div
        className="submitButton"
        style={{ backgroundColor: "#BA68C8" }}
        onClick={() => submitTrans()}
      >
        <p
          style={{
            color: "white",
            fontWeight: "bolder",
            fontSize: 18,
            userSelect: "none",
          }}
        >
          ثبت تراکنش
        </p>
      </div>
    </div>
  );
};

export default Transaction;
