import React, { useEffect, useState } from "react";
import "../App.css";
import { useStore } from "../Store/Store";

const TransactionsList = () => {
  const { setSelected, Translist } = useStore();

  useEffect(() => {
    setSelected(2);
  }, []);

  return (
    <div className="TransContainer">
      {Translist ? (
        Translist.map((item, index) => (
          <div className="TranslistItems">
            <h3>کارت مبدا : {item.cardNum}</h3>
            <h3>مقدار انتقال : {item.transValue}</h3>
            <h3>کارت مقصد : {item.destCard}</h3>
          </div>
        ))
      ) : (
        <div
          style={{
            height: "100%",
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            color : '#b3e5fc'
          }}
        >
          <h1 style={{fontWeight : 'bold',fontSize : 40,userSelect : 'none'}}>تراکنشی وجود ندارد</h1>
        </div>
      )}
    </div>
  );
};

export default TransactionsList;
