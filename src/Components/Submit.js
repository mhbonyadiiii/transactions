import React, { useEffect } from "react";
import "../App.css";
import { useStore } from "../Store/Store";
import CardLogo from "./CardLogo";

const Submit = () => {
  const {
    Carts,
    setCarts,
    Card,
    setCard,
    CVV2,
    setCVV2,
    Exp,
    setExp,
    Owner,
    setOwner,
    Value,
    setValue,
    cardRef,
    cvv2Ref,
    expRef,
    ownerRef,
    valueRef,
    setSelected,
    bankId,
    setBankId,
    valid,
    setValid,
  } = useStore();

  const cartPush = () => {
    let temp = [...Carts];
    if (valid === true) {
      if (
        Card.length == 16 &&
        CVV2.length >= 3 &&
        Exp.length == 4 &&
        Owner.length > 6 &&
        Value.length > 0
      ) {
        const a = Carts.find((ele) => ele.Num === Card);
        if (Carts.includes(a) == false) {
          temp.push({
            Num: Card,
            CVV2: CVV2,
            Exp: Exp,
            Owner: Owner,
            Value: Value,
            Transactions: [],
          });
          setCarts(temp);
          setCard([]);
          cardRef.current.value = "";
          setCVV2([]);
          cvv2Ref.current.value = "";
          setExp([]);
          expRef.current.value = "";
          setOwner([]);
          ownerRef.current.value = "";
          setValue([]);
          valueRef.current.value = "";
          setValid(false);
          setBankId([]);
        } else {
          alert("کارت قبلا ثبت شده است");
        }
      } else {
        alert("اطلاعات کارت را کامل کنید");
      }
    } else {
      alert("اطلاعات کارت صحیح نیست");
    }
  };

  useEffect(() => {
    setSelected(0);
  }, []);

  return (
    <div className="cartInputbox">
      <div
        className="cartInput"
        style={{
          display: "flex",
          flexDirection: "row",
          direction: "rtl",
        }}
      >
        <input
          style={{ textAlign: "left" }}
          className="Input"
          maxLength={16}
          ref={cardRef}
          value={Card}
          type="text"
          onKeyPress={(event) => {
            let char = String.fromCharCode(event.which);
            if (!/[0-9]/.test(char)) {
              event.preventDefault();
            }
          }}
          onChange={(e) => {
            setCard(e.target.value);
            if (e.target.value.length >= 6) {
              setBankId(e.target.value.slice(0, 6));
            } else {
              setBankId([]);
            }
          }}
        />
        <div className="inputTitleBox" style={{ right: 15 }}>
          <p className="inputTitle">شماره کارت</p>
        </div>
        <div
          style={{
            height: "100%",
            width: 70,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CardLogo id={bankId} />
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          maxLength={4}
          value={CVV2}
          type="text"
          onKeyPress={(event) => {
            let char = String.fromCharCode(event.which);
            if (!/[0-9]/.test(char)) {
              event.preventDefault();
            }
          }}
          onChange={(e) => setCVV2(e.target.value)}
          ref={cvv2Ref}
        />
        <div className="inputTitleBox">
          <p className="inputTitle">CVV2</p>
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          maxLength={4}
          onKeyPress={(event) => {
            let char = String.fromCharCode(event.which);
            if (!/[0-9]/.test(char)) {
              event.preventDefault();
            }
          }}
          type="text"
          onChange={(e) => setExp(e.target.value)}
          ref={expRef}
          value={Exp}
        />
        <div className="inputTitleBox">
          <p className="inputTitle">تاریخ انقضا</p>
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          style={{ textAlign: "right" }}
          onChange={(e) => {
            setOwner(e.target.value);
          }}
          ref={ownerRef}
          value={Owner}
        />
        <div className="inputTitleBox">
          <p className="inputTitle">صاحب کارت</p>
        </div>
      </div>
      <div className="cartInput">
        <input
          className="Input"
          onChange={(e) => setValue(e.target.value)}
          ref={valueRef}
          onKeyPress={(event) => {
            let char = String.fromCharCode(event.which);
            if (!/[0-9]/.test(char)) {
              event.preventDefault();
            }
          }}
          type="text"
          value={Value}
        />
        <div className="inputTitleBox">
          <p className="inputTitle">موجودی</p>
        </div>
      </div>
      <div className="submitButton" onClick={() => cartPush()}>
        <p
          style={{
            color: "black",
            fontWeight: "bolder",
            fontSize: 18,
            userSelect: "none",
          }}
        >
          ثبت
        </p>
      </div>
    </div>
  );
};

export default Submit;
