import React from "react";
import "../App.css";
import { useStore } from "../Store/Store";
import Card from "./Card";

const CardsBar = () => {
  const {
    Carts,
    setCarts,
    setCardNum,
    setCVV2Num,
    setExpNum,
    setTranslist,
    cardSelected,
    setCardSelected,
  } = useStore();

  const cardDatapush = (e) => {
    let temp = [...Carts];
    temp.find((ele, index) => {
      if (index == e) {
        setTranslist(ele.Transactions);
      }
    });
    setCarts(temp);
  };

  return (
    <>
      {Carts.length == 0 ? (
        <div className="cartsContainer">
          <h1 style={{ color: "white" }}>کارت بانکی خود را اضافه کنید</h1>
        </div>
      ) : (
        <div className="cartsContainer2">
          {Carts.map((item, index) => (
            <Card
              style={
                index === cardSelected ? { border: "5px white solid",boxShadow : '0px 0px 25px 5px black' } : null
              }
              num={item.Num}
              cvv2={item.CVV2}
              exp={item.Exp}
              value={item.Value}
              owner={item.Owner}
              onClick={() => {
                setCardNum(item.Num);
                setCVV2Num(item.CVV2);
                setExpNum(item.Exp);
                cardDatapush(index);
                setCardSelected(index);
              }}
            />
          ))}
        </div>
      )}
    </>
  );
};

export default CardsBar;
