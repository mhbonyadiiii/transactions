import React, { createContext, useContext, useState, useRef } from "react";

export const store = createContext();

const Store = ({ children }) => {
  const [Carts, setCarts] = useState([]);
  const [Card, setCard] = useState([]);
  const cardRef = useRef();
  const [CVV2, setCVV2] = useState([]);
  const cvv2Ref = useRef();
  const [Exp, setExp] = useState([]);
  const expRef = useRef();
  const [Owner, setOwner] = useState([]);
  const ownerRef = useRef();
  const [Value, setValue] = useState([]);
  const valueRef = useRef();
  const [Menu, setMenu] = useState([
    { name: "ثبت کارت", link: "/" },
    { name: "ثبت تراکنش", link: "/transactions" },
    { name: "تراکنش ها", link: "/TransactionsList" },
  ]);
  const [selected, setSelected] = useState(0);
  const [cardSelected, setCardSelected] = useState(0);
  const [CardNum, setCardNum] = useState();
  const [CVV2Num, setCVV2Num] = useState();
  const [ExpNum, setExpNum] = useState();
  const [transValue, setTransValue] = useState();
  const [DestCard, setDestCard] = useState([]);
  const [Password, setPassword] = useState([]);
  const [Translist, setTranslist] = useState();
  const [bankId, setBankId] = useState([]);
  const [valid, setValid] = useState(false);

  const passRef = useRef();
  const transRef = useRef();
  const destRef = useRef();
  const CardNumRef = useRef();
  const CVV2NumRef = useRef();
  const ExpNumRef = useRef();

  const allValueCompute = (id, property, newValue) => {
    let temp = [...Carts];
    temp.find((elem) => elem.Num === id)[property] = newValue;
    setCarts(temp);
  };

  return (
    <store.Provider
      value={{
        Carts,
        setCarts,
        Card,
        setCard,
        CVV2,
        setCVV2,
        Exp,
        setExp,
        Owner,
        setOwner,
        Value,
        setValue,
        cardRef,
        cvv2Ref,
        expRef,
        ownerRef,
        valueRef,
        Menu,
        setMenu,
        selected,
        setSelected,
        CardNum,
        setCardNum,
        CVV2Num,
        setCVV2Num,
        ExpNum,
        setExpNum,
        transValue,
        setTransValue,
        DestCard,
        setDestCard,
        allValueCompute,
        Password,
        setPassword,
        passRef,
        transRef,
        destRef,
        CardNumRef,
        CVV2NumRef,
        ExpNumRef,
        Translist,
        setTranslist,
        bankId,
        setBankId,
        valid,
        setValid,
        cardSelected,
        setCardSelected,
      }}
    >
      {children}
    </store.Provider>
  );
};
const useStore = () => {
  return useContext(store);
};
export { useStore };

export default Store;
