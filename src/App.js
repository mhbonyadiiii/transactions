import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import RouterMenu from "./Components/RouterMenu";
import CardsBar from "./Components/CardsBar";
import Submit from "./Components/Submit";
import Transaction from "./Components/Transaction";
import TransactionsList from "./Components/TransactionsList";

const App = () => {
  return (
    <Router>
      <div className="Container">
        <CardsBar />
        <RouterMenu />
        <Switch>
          <Route exact path='/'>
            <Submit />
          </Route>
          <Route path='/transactions'>
            <Transaction/>
          </Route>
          <Route path='/TransactionsList'>
            <TransactionsList/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
